package summultiples

func SumMultiples(limit int, divisors ...int) int {
	mapping := make(map[int]bool)
	for _, divisor := range divisors {

		for i := divisor; i < limit; i += divisor {
			//fmt.Print(i)
			mapping[i] = true
			if i == 0 {
				break
			}
		}
	}
	result := 0
	for k := range mapping {
		result += k
	}
	return result
}
