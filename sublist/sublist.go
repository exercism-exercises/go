package sublist

// Relation type is defined in relations.go file.

func Sublist(l1, l2 []int) Relation {
	n1 := len(l1)
	n2 := len(l2)
	switch {
	case n1 == 0 && n2 == 0:
		return RelationEqual
	case n1 == 0 && n2 != 0:
		return RelationSublist
	case n1 != 0 && n2 == 0:
		return RelationSuperlist
	case n1 != 0 && n2 != 0 && n1 < n2:
		for j, e2 := range l2 {
			result := e2 == l1[0]
			if result {
				for i := 1; i < n1; i++ {
					result = result && l1[i] == l2[j+i]
					if !result {
						break
					}
				}
				if result {
					return RelationSublist
				}
			}
		}
		return RelationUnequal
	case n1 != 0 && n2 != 0 && n1 > n2:
		for i, e1 := range l1 {
			result := e1 == l2[0]
			if result {
				for j := 1; j < n2; j++ {
					result = result && l1[i+j] == l2[j]
					if !result {
						break
					}
				}
				if result {
					return RelationSuperlist
				}
			}
		}
		return RelationUnequal
	default:
		for i := 0; i < n1; i++ {
			if l1[i] != l2[i] {
				return RelationUnequal
			}
		}
		return RelationEqual
	}
}
