package birdwatcher

// TotalBirdCount return the total bird count by summing
// the individual day's counts.
func TotalBirdCount(birdsPerDay []int) int {
	totalCount := 0
	for _, count := range birdsPerDay {
		totalCount += count
	}
	return totalCount
}

// BirdsInWeek returns the total bird count by summing
// only the items belonging to the given week.
func BirdsInWeek(birdsPerDay []int, week int) int {
	weekCount := 0
	dayFrom := (week - 1) * 7
	dayEnd := dayFrom
	if dayFrom+7 < len(birdsPerDay) {
		dayEnd += 7
	} else {
		dayEnd = len(birdsPerDay)
	}
	for i := dayFrom; i < dayEnd; i++ {
		weekCount += birdsPerDay[i]
	}
	return weekCount
}

// FixBirdCountLog returns the bird counts after correcting
// the bird counts for alternate days.
func FixBirdCountLog(birdsPerDay []int) []int {
	for i := 0; i < len(birdsPerDay); i += 2 {
		birdsPerDay[i]++
	}
	return birdsPerDay
}
