package collatzconjecture

import "errors"

func CollatzConjecture(n int) (int, error) {
	if n < 1 {
		return 0, errors.New("invalid number")
	}
	count := 0
	for {
		if n == 1 {
			break
		} else if n%2 == 0 {
			count += 1
			n /= 2
		} else {
			count += 1
			n *= 3
			n += 1
		}
	}
	return count, nil
}
