// This is a "stub" file.  It's a little start on your solution.
// It's not a complete solution though; you have to write some code.

// Package acronym should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package acronym

import (
	"regexp"
	"strings"
)

/*
*
abbreviates a word to acronym
*/
func Abbreviate(s string) string {
	re := regexp.MustCompile(`\p{S}+`)
	s = strings.ReplaceAll(s, "-", " ")
	words := strings.Split(s, " ")
	response := make([]rune, 0)
	for _, word := range words {
		word = strings.Trim(word, " _")
		word = re.ReplaceAllString(word, "")
		if len(word) > 0 {
			letters := []rune(strings.ToUpper(word))
			response = append(response, letters[0])

		}
	}
	return string(response)
}
