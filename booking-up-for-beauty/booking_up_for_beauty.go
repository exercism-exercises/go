package booking

import (
	"fmt"
	"time"
)

// Schedule returns a time.Time from a string containing a date.
func Schedule(date string) time.Time {
	schedule, err := time.Parse("1/2/2006 15:04:05", date)
	if err != nil {
		panic("Parse failed " + err.Error())
	}
	return schedule
}

// HasPassed returns whether a date has passed.
func HasPassed(date string) bool {
	//"July 25, 2019 13:45:00"
	schedule, err := time.Parse("January 2, 2006 15:04:05", date)
	if err != nil {
		panic("Parse failed " + err.Error())
	}
	return schedule.Before(time.Now())
}

// IsAfternoonAppointment returns whether a time is in the afternoon.
func IsAfternoonAppointment(date string) bool {
	//"Monday, 02-Jan-06 15:04:05 MST"
	//"Thursday, May 13, 2010 20:32:00"
	schedule, err := time.Parse("Monday, January 2, 2006 15:04:05", date)
	if err != nil {
		panic("Parse failed " + err.Error())
	}
	hour := schedule.Hour()
	return 12 <= hour && hour < 18

}

// Description returns a formatted string of the appointment time.
func Description(date string) string {
	return fmt.Sprintf("You have an appointment on %s.", Schedule(date).Format("Monday, January 2, 2006, at 15:04"))
}

// AnniversaryDate returns a Time with this year's anniversary.
func AnniversaryDate() time.Time {
	schedule, err := time.Parse(time.DateOnly, "2020-09-15")
	if err != nil {
		panic("Parse failed " + err.Error())
	}
	yearDiff := time.Now().Year() - schedule.Year()
	return schedule.AddDate(yearDiff, 0, 0)

}
