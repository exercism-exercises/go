package complexnumbers

import "math"

// Define the Number type here.
type Number struct {
	real, imaginary float64
}

func (n Number) Real() float64 {
	return n.real
}

func (n Number) Imaginary() float64 {
	return n.imaginary
}

func (n1 Number) Add(n2 Number) Number {
	return Number{real: n1.Real() + n2.Real(), imaginary: n1.Imaginary() + n2.Imaginary()}
}

func (n1 Number) Subtract(n2 Number) Number {
	return Number{real: n1.Real() - n2.Real(), imaginary: n1.Imaginary() - n2.Imaginary()}
}

func (n1 Number) Multiply(n2 Number) Number {
	return Number{real: n1.Real()*n2.Real() - n1.Imaginary()*n2.Imaginary(), imaginary: n1.Real()*n2.Imaginary() + n1.Imaginary()*n2.Real()}
}

func (n Number) Times(factor float64) Number {
	return Number{real: n.Real() * factor, imaginary: n.Imaginary() * factor}
}

func (n1 Number) Divide(n2 Number) Number {
	/**
	(a + i * b) / (c + i * d) = (a * c + b * d)/(c^2 + d^2) + (b * c - a * d)/(c^2 + d^2) * i
	*/
	return Number{
		real:      (n1.Real()*n2.Real() + n1.Imaginary()*n2.Imaginary()) / (n2.Real()*n2.Real() + n2.Imaginary()*n2.Imaginary()),
		imaginary: (n1.Imaginary()*n2.Real() - n1.Real()*n2.Imaginary()) / (n2.Real()*n2.Real() + n2.Imaginary()*n2.Imaginary()),
	}

}

func (n Number) Conjugate() Number {
	return Number{real: n.Real(), imaginary: -n.Imaginary()}
}

func (n Number) Abs() float64 {
	return math.Sqrt(n.Real()*n.Real() + n.Imaginary()*n.Imaginary())
}

func (n Number) Exp() Number {
	return Number{real: math.Cos(n.Imaginary()), imaginary: math.Sin(n.Imaginary())}.Times(math.Exp(n.Real()))
}
