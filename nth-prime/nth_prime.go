package prime

import "errors"

// Nth returns the nth prime number. An error must be returned if the nth prime number can't be calculated ('n' is equal or less than zero)
func Nth(n int) (int, error) {
	if n < 1 {
		return 0, errors.New("invalid range")
	}
	m := 2
	primes := []int{m}
	for {
		if len(primes) == n {
			return primes[n-1], nil
		}
		m++
		is_m_prime := true
		for _, p := range primes {
			is_m_prime = is_m_prime && m%p != 0
		}

		if is_m_prime {
			primes = append(primes, m)
		}
	}

}
