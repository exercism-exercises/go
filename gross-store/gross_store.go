package gross

// Units stores the Gross Store unit measurements.
func Units() map[string]int {
	/*
		| quarter_of_a_dozen | 3     |
		| half_of_a_dozen    | 6     |
		| dozen              | 12    |
		| small_gross        | 120   |
		| gross              | 144   |
		| great_gross        | 1728  |

	*/
	return map[string]int{
		"quarter_of_a_dozen": 3,
		"half_of_a_dozen":    6,
		"dozen":              12,
		"small_gross":        120,
		"gross":              144,
		"great_gross":        1728,
	}
}

// NewBill creates a new bill.
func NewBill() map[string]int {
	return make(map[string]int)
}

// AddItem adds an item to customer bill.
func AddItem(bill, units map[string]int, item, unit string) bool {
	value_from_unit, exists := units[unit]
	if !exists {
		return false
	}
	_, exists = bill[item]
	if exists {
		bill[item] += value_from_unit
	} else {
		bill[item] = value_from_unit
	}
	return true
}

// RemoveItem removes an item from customer bill.
func RemoveItem(bill, units map[string]int, item, unit string) bool {
	value_from_unit, unit_exists := units[unit]
	value_from_bill, item_exists := bill[item]
	if !unit_exists || !item_exists || value_from_bill < value_from_unit {
		return false
	}
	if value_from_unit == value_from_bill {
		delete(bill, item)
	} else {
		bill[item] -= value_from_unit
	}
	return true
}

// GetItem returns the quantity of an item that the customer has in his/her bill.
func GetItem(bill map[string]int, item string) (int, bool) {
	item_value, item_exists := bill[item]
	return item_value, item_exists
}
