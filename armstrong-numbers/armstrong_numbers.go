package armstrong

import (
	"fmt"
	"math"
)

func IsNumber(n int) bool {
	s := fmt.Sprint(n)
	p := len(s)
	result := 0

	for _, k := range s {
		result += int(math.Pow(float64(k-'0'), float64(p)))
	}
	return n == result
}
