package stringset

import (
	"strings"
)

// Implement Set as a collection of unique string values.
//
// For Set.String, use '{' and '}', output elements as double-quoted strings
// safely escaped with Go syntax, and use a comma and a single space between
// elements. For example, a set with 2 elements, "a" and "b", should be formatted as {"a", "b"}.
// Format the empty set as {}.

// Define the Set type here.
type Set map[string]bool

func New() Set {
	return make(Set)
}

func NewFromSlice(l []string) Set {
	s := make(Set, len(l))
	for _, word := range l {
		s[word] = true
	}
	return s
}

func (s Set) String() string {
	slice := make([]string, 0)
	for k := range s {
		slice = append(slice, "\""+k+"\"")
	}
	return "{" + strings.Join(slice, ", ") + "}"
}

func (s Set) IsEmpty() bool {
	return len(s) == 0
}

func (s Set) Has(elem string) bool {
	_, ok := s[elem]
	return ok
}

func (s Set) Add(elem string) {
	s[elem] = true
}

func Subset(s1, s2 Set) bool {
	result := true
	for k := range s1 {
		_, ok := s2[k]
		result = result && ok
	}
	return result
}

func Disjoint(s1, s2 Set) bool {
	result := true
	for k := range s1 {
		_, ok := s2[k]
		result = result && !ok
	}
	return result
}

func Equal(s1, s2 Set) bool {
	return Subset(s1, s2) && len(s1) == len(s2)
}

func Intersection(s1, s2 Set) Set {
	s3 := make(Set)
	for k := range s1 {
		_, ok := s2[k]
		if ok {
			s3[k] = ok
		}
	}
	return s3
}

func Difference(s1, s2 Set) Set {
	s3 := make(Set)
	for k := range s1 {
		_, ok := s2[k]
		if !ok {
			s3[k] = true
		}
	}
	return s3
}

func Union(s1, s2 Set) Set {
	s3 := make(Set)
	for k := range s1 {
		s3[k] = true
	}
	for k := range s2 {
		s3[k] = true
	}
	return s3
}
