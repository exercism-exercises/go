package cryptosquare

import (
	"math"
	"strings"
	"unicode"
)

func Encode(pt string) string {
	if len(pt) == 0 {
		return ""
	}
	normalized := make([]rune, 0)
	for _, l := range strings.ToLower(pt) {
		if unicode.IsDigit(l) || unicode.IsLetter(l) {
			normalized = append(normalized, l)
		}
	}
	c := int(math.Ceil(math.Sqrt(float64(len(normalized)))))
	r := c
	if c*(c-1) >= len(normalized) {
		r -= 1
	}

	for j := len(normalized); j < r*c; j++ {
		normalized = append(normalized, ' ')
	}

	rectangle := make([][]rune, r)

	for i := 0; i < r; i++ {
		row := make([]rune, c)
		for j := 0; j < c; j++ {
			row[j] = normalized[i*c+j]
		}
		rectangle[i] = row
	}

	result := make([]string, c)
	for j := 0; j < c; j++ {
		col := make([]rune, r)
		for i := 0; i < r; i++ {
			col[i] = rectangle[i][j]
		}
		line := string(col)
		result[j] = line
	}

	return strings.Join(result, " ")
}
