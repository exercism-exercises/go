package tournament

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"slices"
	"strings"
)

func Tally(reader io.Reader, writer io.Writer) error {
	matchesPlayed := make(map[string]int)
	matchesWon := make(map[string]int)
	matchesLost := make(map[string]int)
	matchesDrawn := make(map[string]int)
	tally := make(map[string]int)
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		raw := scanner.Text()
		if len(strings.Trim(raw, " \n")) == 0 || strings.HasPrefix(raw, "#") {
			continue
		}
		line := strings.Split(raw, ";")
		if len(line) != 3 {
			return errors.New("wrong format")
		}
		home := line[0]
		away := line[1]
		result := line[2]
		_, homeOk := matchesPlayed[home]
		_, awayOk := matchesPlayed[away]
		if !homeOk {
			matchesPlayed[home] = 0
			tally[home] = 0
			matchesWon[home] = 0
			matchesLost[home] = 0
			matchesDrawn[home] = 0

		}
		if !awayOk {
			matchesPlayed[away] = 0
			tally[away] = 0
			matchesWon[away] = 0
			matchesLost[away] = 0
			matchesDrawn[away] = 0
		}
		matchesPlayed[home] += 1
		matchesPlayed[away] += 1
		switch result {
		case "win":
			matchesLost[away] += 1
			matchesWon[home] += 1
			tally[home] += 3
		case "loss":
			tally[away] += 3
			matchesLost[home] += 1
			matchesWon[away] += 1
		case "draw":
			matchesDrawn[home] += 1
			matchesDrawn[away] += 1
			tally[home] += 1
			tally[away] += 1
		default:
			return errors.New("invalid result")
		}

	}
	teams := make([]string, 0)
	for team := range matchesPlayed {
		teams = append(teams, team)
	}
	slices.SortFunc(teams, func(a, b string) int {
		aPoint := tally[a]
		bPoint := tally[b]
		if aPoint > bPoint {
			return -1
		} else if aPoint < bPoint {
			return 1
		}

		return strings.Compare(a, b)
	})
	writer.Write([]byte("Team                           | MP |  W |  D |  L |  P\n"))

	for _, team := range teams {

		s := fmt.Sprintf(
			"%-30s | %2d | %2d | %2d | %2d | %2d\n",
			team,
			matchesPlayed[team],
			matchesWon[team],
			matchesDrawn[team],
			matchesLost[team],
			tally[team],
		)

		writer.Write([]byte(s))

	}

	return nil
}
