package anagram

import (
	"reflect"
	"strings"
)

func Detect(subject string, candidates []string) []string {
	subject = strings.ToLower(subject)
	sampleCount := Count(subject)
	result := make([]string, 0)
	for _, word := range candidates {
		wordCount := Count(word)
		if subject != strings.ToLower(word) && reflect.DeepEqual(sampleCount, wordCount) {
			result = append(result, word)
		}
	}
	return result
}

func Count(word string) map[rune]int {
	result := map[rune]int{}
	for _, r := range strings.ToLower(word) {
		result[r]++
	}

	return result
}
