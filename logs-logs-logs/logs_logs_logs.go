package logs

import (
	"strings"
	"unicode/utf8"
)

var rune_to_application map[rune]string = map[rune]string{
	'❗': "recommendation",
	'🔍': "search",
	'☀': "weather",
}

// Application identifies the application emitting the given log.
func Application(log string) string {
	for _, char := range log {
		application, exists := rune_to_application[char]
		if exists {
			return application
		}
	}
	return "default"
}

// Replace replaces all occurrences of old with new, returning the modified log
// to the caller.
func Replace(log string, oldRune, newRune rune) string {
	return strings.ReplaceAll(log, string(oldRune), string(newRune))
}

// WithinLimit determines whether or not the number of characters in log is
// within the limit.
func WithinLimit(log string, limit int) bool {
	// utf8.RuneCountInString(log)
	// limit is inclusive
	return utf8.RuneCountInString(log) <= limit
}
