package series

func All(n int, s string) []string {
	result := make([]string, 0)
	t := []rune(s)
	if len(s) >= n {
		for i := 0; i <= len(s)-n; i++ {
			result = append(result, string(t[i:i+n]))
		}
	}
	return result
}

func UnsafeFirst(n int, s string) string {
	if len(s) < n {
		return ""
	}
	t := []rune(s)
	return string(t[:n])
}

func First(n int, s string) (first string, ok bool) {
	if len(s) < n {
		return "", false
	}
	t := []rune(s)
	return string(t[:n]), true
}
