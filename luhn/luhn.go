package luhn

import (
	"strings"
	"unicode"
	"strconv"
)

func Valid(id string) bool {
	id = strings.ReplaceAll(id, " ", "")
	n := len(id)
	result := 0
	parity := n % 2
	if n < 2 {
		return false
	}
	for i, c := range id  {
		if unicode.IsDigit(c) {
			d, _ := strconv.Atoi(string(c))
			if i % 2 == parity {
				d *= 2
				if d > 9 {
					d -= 9
				}
			}
			result += d
		} else {
			return false
		}
	}
	return result % 10 == 0
}
