package atbash

import (
	"strings"
	"unicode"
)

func Atbash(s string) string {
	text := make([]rune, 0)
	for _, l := range s {
		if unicode.IsLetter(l) || unicode.IsDigit(l) {
			text = append(text, l)
		}
	}
	result := make([]rune, 0)

	for j, l := range strings.ToLower(string(text)) {
		if unicode.IsLetter(l) {
			result = append(result, rune(25-int(l-'a')+'a'))
		} else {
			result = append(result, l)
		}
		if j < len(text)-1 && j%5 == 4 {
			result = append(result, ' ')
		}
	}

	return string(result)
}
