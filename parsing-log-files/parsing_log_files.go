package parsinglogfiles

import (
	"regexp"
	"strings"
)

/*
*
- [TRC]
- [DBG]
- [INF]
- [WRN]
- [ERR]
- [FTL]
*/
func IsValidLine(text string) bool {
	is_valid, _ := regexp.MatchString(`^\[(TRC|DBG|INF|WRN|ERR|FTL)\]`, text)
	return is_valid
}

/*
*
"~", "\*", "=" and "-"
*/
func SplitLogLine(text string) []string {
	re := regexp.MustCompile(`<(~|\*|=|-)*>`)
	return re.Split(text, -1)
}

func CountQuotedPasswords(lines []string) int {
	re := regexp.MustCompile(`(?i)".*password.*"`)
	count := 0
	for _, line := range lines {
		if re.MatchString(line) {
			count += 1
		}
	}
	return count
}

func RemoveEndOfLineText(text string) string {
	re := regexp.MustCompile(`end-of-line[0-9]*`)
	return re.ReplaceAllString(text, "")
}

func TagWithUserName(lines []string) []string {
	taggedLines := make([]string, len(lines))
	re := regexp.MustCompile(`User[[:space:]]+[[:alnum:]]+`)
	for i, line := range lines {
		userName := re.FindString(line)
		if len(userName) > 0 {
			userNameSplit := strings.Fields(userName)
			taggedLines[i] = "[USR] " + userNameSplit[len(userNameSplit)-1] + " " + line
		} else {
			taggedLines[i] = line
		}
	}
	return taggedLines
}
