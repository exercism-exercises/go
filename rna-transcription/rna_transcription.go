package strand

func ToRNA(dna string) string {
	var dnaToRna = map[rune]rune{
		'G': 'C',
		'C': 'G',
		'T': 'A',
		'A': 'U',
	}

	rna := make([]rune, len(dna))

	for i, nucleotide := range(dna) {
		rna_nucleotide, ok := dnaToRna[nucleotide]
		if ok {
			rna[i] = rna_nucleotide
		} else {
			return ""
		}
	}

	return string(rna)
}
