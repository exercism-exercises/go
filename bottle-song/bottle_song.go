package bottlesong

func verse(bottles int) []string {
	switch bottles {
	case 10:
		return []string{"Ten green bottles hanging on the wall,", "Ten green bottles hanging on the wall,", "And if one green bottle should accidentally fall,", "There'll be nine green bottles hanging on the wall."}
	case 9:
		return []string{"Nine green bottles hanging on the wall,", "Nine green bottles hanging on the wall,", "And if one green bottle should accidentally fall,", "There'll be eight green bottles hanging on the wall."}
	case 8:
		return []string{"Eight green bottles hanging on the wall,", "Eight green bottles hanging on the wall,", "And if one green bottle should accidentally fall,", "There'll be seven green bottles hanging on the wall."}
	case 7:
		return []string{"Seven green bottles hanging on the wall,", "Seven green bottles hanging on the wall,", "And if one green bottle should accidentally fall,", "There'll be six green bottles hanging on the wall."}
	case 6:
		return []string{"Six green bottles hanging on the wall,", "Six green bottles hanging on the wall,", "And if one green bottle should accidentally fall,", "There'll be five green bottles hanging on the wall."}
	case 5:
		return []string{"Five green bottles hanging on the wall,", "Five green bottles hanging on the wall,", "And if one green bottle should accidentally fall,", "There'll be four green bottles hanging on the wall."}
	case 4:
		return []string{"Four green bottles hanging on the wall,", "Four green bottles hanging on the wall,", "And if one green bottle should accidentally fall,", "There'll be three green bottles hanging on the wall."}
	case 3:
		return []string{"Three green bottles hanging on the wall,", "Three green bottles hanging on the wall,", "And if one green bottle should accidentally fall,", "There'll be two green bottles hanging on the wall."}
	case 2:
		return []string{"Two green bottles hanging on the wall,", "Two green bottles hanging on the wall,", "And if one green bottle should accidentally fall,", "There'll be one green bottle hanging on the wall."}
	case 1:
		return []string{"One green bottle hanging on the wall,", "One green bottle hanging on the wall,", "And if one green bottle should accidentally fall,", "There'll be no green bottles hanging on the wall."}
	default:
		return []string{}
	}

}

func Recite(startBottles, takeDown int) []string {
	verses := verse(startBottles)
	for i := 1; i < takeDown; i++ {
		verses = append(verses, "")
		verses = append(verses, verse(startBottles-i)...)
	}
	return verses
}
