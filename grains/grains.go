package grains

import (
	"errors"
	"math"
)

func Square(number int) (uint64, error) {
	if number < 1 || number > 64 {
		return 0, errors.New("invalid square")
	}
	var value uint64 = 1 << (number - 1)
	return value, nil
}

func Total() uint64 {
	return math.MaxUint64
}
