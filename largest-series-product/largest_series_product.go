package lsproduct

import (
	"errors"
	"unicode"
)

func LargestSeriesProduct(digits string, span int) (int64, error) {
	letters := []rune(digits)
	if len(letters) < span || span <= 0 {
		return 0, errors.New("invalid number of digits")
	}

	result := int64(0)
	for i := 0; i < len(letters)-span+1; i++ {
		value := int64(0)
		if unicode.IsDigit(letters[i]) {
			value = int64(int(letters[i] - '0'))
		} else {
			return 0, errors.New("invalid rune found")
		}
		for j := i + 1; j < i+span; j++ {
			if unicode.IsDigit(letters[j]) {
				value *= int64(int(letters[j] - '0'))
			} else {
				return 0, errors.New("invalid rune found")
			}
		}
		if value > result {
			result = value
		}
	}

	return result, nil
}
