package clock

import "fmt"

// Define the Clock type here.

type Clock struct {
	minute int
}

func New(h, m int) Clock {
	return Clock{minute: (((h*60 + m) % 1440) + 1440) % 1440}
}

func (c Clock) Add(m int) Clock {
	return Clock{minute: ((c.minute+m)%1440 + 1440) % 1440}
}

func (c Clock) Subtract(m int) Clock {
	return Clock{minute: ((c.minute-m)%1440 + 1440) % 1440}
}

func (c Clock) String() string {
	hour := c.minute / 60
	minute := c.minute % 60
	return fmt.Sprintf("%02d:%02d", hour, minute)
}
