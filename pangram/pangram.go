package pangram

import "strings"

var alphabet string = "abcdefghijklmnopqrstuvwxyz"

func IsPangram(input string) bool {
	chars := make(map[rune]bool, 26)
	word := strings.ToLower(input)
	for _, char := range word {
		chars[char] = true
	}

	for _, char := range alphabet {
		_, exists := chars[char]
		if !exists {
			return false
		}
	}
	return true
}
