package phonenumber

import (
	"errors"
	"unicode"
)

func Number(phoneNumber string) (string, error) {
	text := make([]rune, 0)

	for _, l := range phoneNumber {
		if unicode.IsDigit(l) {
			text = append(text, l)
		}
	}
	n := len(text)
	if n != 10 && n != 11 {
		return "", errors.New("Invalid phone number")
	} else if n == 11 {
		if text[0] != '1' {
			return "", errors.New("Invalid phone number")
		} else if text[1] < '2' || text[4] < '2' {
			return "", errors.New("Invalid phone number")
		}
		return string(text[1:]), nil
	} else {
		if text[0] < '2' || text[3] < '2' {
			return "", errors.New("Invalid phone number")
		}
		return string(text), nil
	}
}

func AreaCode(phoneNumber string) (string, error) {
	number, err := Number(phoneNumber)
	if err != nil {
		return "", errors.New("Invalid phone number")
	}
	return string([]rune(number)[0:3]), nil
}

func Format(phoneNumber string) (string, error) {
	number, err := Number(phoneNumber)
	if err != nil {
		return "", errors.New("Invalid phone number")
	}
	letters := []rune(number)
	formatted := make([]rune, 14)
	formatted[0] = '('
	formatted[4] = ')'
	formatted[5] = ' '
	formatted[9] = '-'
	for i := 0; i < 3; i++ {
		formatted[i+1] = letters[i]
	}
	for i := 3; i < 6; i++ {
		formatted[i+3] = letters[i]
	}
	for i := 6; i < 10; i++ {
		formatted[i+4] = letters[i]
	}
	return string(formatted), nil
}
