package romannumerals

import (
	"errors"
	"strings"
)

/*
*

| M    | D   | C   | L   | X   | V   | I   |
| ---- | --- | --- | --- | --- | --- | --- |
| 1000 | 500 | 100 | 50  | 10  | 5   | 1   |

	4 (`IV`), 9 (`IX`), 40 (`XL`), 90 (`XC`), 400 (`CD`) and 900 (`CM`)
*/

func ResolveDigit(digit int, oneRomanNumeral string, fiveRomanNumeral string, tenRomanNumeral string) string {

	if 0 < digit && digit < 4 {
		return strings.Repeat(oneRomanNumeral, digit)
	} else if digit == 4 {
		return oneRomanNumeral + fiveRomanNumeral
	} else if 4 < digit && digit < 9 {
		return fiveRomanNumeral + strings.Repeat(oneRomanNumeral, digit-5)
	} else if digit == 9 {
		return oneRomanNumeral + tenRomanNumeral
	}
	return ""
}

func ToRomanNumeral(input int) (string, error) {
	// MMMCMXCIX (or 3,999)
	if input < 1 || input > 3999 {
		return "", errors.New("invalid range")
	}

	return ResolveDigit(input/1000, "M", "", "") + ResolveDigit((input%1000)/100, "C", "D", "M") + ResolveDigit((input%100)/10, "X", "L", "C") + ResolveDigit((input%10), "I", "V", "X"), nil
}
