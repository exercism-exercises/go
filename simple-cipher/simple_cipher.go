package cipher

import (
	"strings"
	"unicode"
)

// Define the shift and vigenere types here.
// Both types should satisfy the Cipher interface.

type shift struct {
	distance int
}

type vigenere struct {
	key []int
}

func NewCaesar() Cipher {
	return NewShift(3)
}

func NewShift(distance int) Cipher {
	if distance < -25 || distance == 0 || distance > 25 {
		return nil
	}
	return shift{distance: distance}
}

func (c shift) Encode(input string) string {
	text := make([]rune, 0)
	for _, l := range strings.ToLower(input) {
		if unicode.IsLetter(l) {
			text = append(text, l)
		}
	}

	result := make([]rune, len(text))

	for i, l := range text {
		result[i] = rune((int(l-'a')+c.distance+26)%26 + 'a')
	}

	return string(result)
}

func (c shift) Decode(input string) string {
	result := make([]rune, len(input))

	for i, l := range input {
		result[i] = rune((int(l-'a')-c.distance+26)%26 + 'a')
	}

	return string(result)
}

func NewVigenere(key string) Cipher {
	if len(key) == 0 {
		return nil
	}
	s := 0
	codes := make([]int, len(key))
	for i, l := range key {
		if unicode.IsLower(l) {
			k := int(unicode.ToLower(l)-'a') % 26
			codes[i] = k
			s += k
		} else {
			return nil
		}
	}

	if s == 0 {
		return nil
	}

	return vigenere{
		key: codes,
	}
}

func (v vigenere) Encode(input string) string {
	result := make([]rune, 0)
	i := 0
	for _, l := range strings.ToLower(input) {
		if unicode.IsLetter(l) {
			result = append(result, rune((int(l-'a')+v.key[i%len(v.key)]+26)%26+'a'))
			i += 1
		}
	}
	return string(result)
}

func (v vigenere) Decode(input string) string {
	result := make([]rune, 0)
	i := 0
	for _, l := range strings.ToLower(input) {
		if unicode.IsLetter(l) {
			result = append(result, rune((int(l-'a')-v.key[i%len(v.key)]+26)%26+'a'))
			i += 1
		}
	}
	return string(result)
}
