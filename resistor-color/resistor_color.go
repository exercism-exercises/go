package resistorcolor

/**
- Black: 0
- Brown: 1
- Red: 2
- Orange: 3
- Yellow: 4
- Green: 5
- Blue: 6
- Violet: 7
- Grey: 8
- White: 9
*/

// Colors returns the list of all colors.
func Colors() []string {
	colors := []string{"black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white"}
	return colors
}

// ColorCode returns the resistance value of the given color.
func ColorCode(color string) int {
	for i, colorValue := range Colors() {
		if color == colorValue {
			return i
		}
	}
	return -1
}
