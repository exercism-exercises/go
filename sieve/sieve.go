package sieve

func Sieve(limit int) []int {
	sieve := make([]bool, limit+1)
	sieve[0] = true
	sieve[1] = true

	for i := 2; i*i <= limit; i++ {
		for j := i * 2; j <= limit; j += i {
			sieve[j] = true
		}
	}

	result := make([]int, 0)
	for i, v := range sieve {
		if !v {
			result = append(result, i)
		}
	}

	return result
}
