package isbn

import (
	"strings"
	"unicode"
)

func IsValidISBN(isbn string) bool {
	chars := []rune(strings.ReplaceAll(isbn, "-", ""))
	if len(chars) != 10 {
		return false
	}
	sum := 0
	for i, c := range chars {
		if unicode.IsDigit(c) {
			sum += ((int(c) - '0') * (10 - i))
		} else if i == 9 && c == 'X' {
			sum += 10
		} else {
			return false
		}
	}
	return sum%11 == 0
}
