package wordcount

import (
	"strings"
	"unicode"
)

type Frequency map[string]int

func WordCount(phrase string) Frequency {
	frequency := make(Frequency, 0)
	for _, word := range strings.FieldsFunc(phrase, func(r rune) bool {
		return unicode.IsSpace(r) || (unicode.IsPunct(r) && r != '\'') || unicode.IsSymbol(r)
	}) {
		word = strings.Trim(word, "'")
		word = strings.ToLower(word)
		//word = replacer.Replace(word)
		if len(word) > 0 {
			frequency[word]++
		}
	}
	return frequency
}
