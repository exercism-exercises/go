package etl

import "strings"

func Transform(in map[int][]string) map[string]int {
	out := make(map[string]int, 0)
	for k, v := range in {
		for _, word := range v {
			out[strings.ToLower(word)] = k
		}
	}
	return out
}
