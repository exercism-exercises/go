package listops

// IntList is an abstraction of a list of integers which we can define methods on
type IntList []int

func (s IntList) Foldl(fn func(int, int) int, initial int) int {
	for _, v := range s {
		initial = fn(initial, v)
	}
	return initial
}

func (s IntList) Foldr(fn func(int, int) int, initial int) int {
	//([1,2,3], x / y, 3) => 1/(2/(3/3))
	// ([3,2,1], y/x, 3)
	return s.Reverse().Foldl(func(x, y int) int { return fn(y, x) }, initial)
}

func (s IntList) Filter(fn func(int) bool) IntList {
	result := make(IntList, 0)

	for _, v := range s {
		if fn(v) {
			result = append(result, v)
		}
	}
	return result
}

func (s IntList) Length() int {
	return len(s)
}

func (s IntList) Map(fn func(int) int) IntList {
	result := make(IntList, s.Length())

	for i, v := range s {
		result[i] = fn(v)
	}
	return result
}

func (s IntList) Reverse() IntList {
	result := make(IntList, s.Length())
	for i := s.Length() - 1; i > -1; i-- {
		result[s.Length()-1-i] = s[i]
	}
	return result
}

func (s IntList) Append(lst IntList) IntList {

	return append(s, lst...)
}

func (s IntList) Concat(lists []IntList) IntList {
	for _, lst := range lists {
		s = s.Append(lst)
	}
	return s
}
