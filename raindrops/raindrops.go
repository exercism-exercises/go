package raindrops

import "fmt"

func Convert(number int) string {
	message := ""

	if number%3 == 0 {
		message += "Pling"
	}

	if number%5 == 0 {
		message += "Plang"
	}

	if number%7 == 0 {
		message += "Plong"
	}
	switch message {
	case "":
		return fmt.Sprintf("%d", number)
	default:
		return message
	}
}
