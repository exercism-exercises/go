package reverse

import "slices"

func Reverse(input string) string {
	input_slice := []rune(input)
	slices.Reverse(input_slice)
	return string(input_slice)
}
