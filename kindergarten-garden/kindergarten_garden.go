package kindergarten

import (
	"errors"
	"sort"
	"strings"
)

// Define the Garden type here.
type Garden map[string][]string

// The diagram argument starts each row with a '\n'.  This allows Go's
// raw string literals to present diagrams in source code nicely as two
// rows flush left, for example,
//
//     diagram := `
//     VVCCGG
//     VVCCGG`

func Plant(r rune) (string, bool) {
	switch r {
	case 'V':
		return "violets", true
	case 'C':
		return "clover", true
	case 'R':
		return "radishes", true
	case 'G':
		return "grass", true
	default:
		return "", false
	}
}

func NewGarden(diagram string, children []string) (*Garden, error) {
	copyChildren := make([]string, len(children))
	copy(copyChildren, children)
	sort.Strings(sort.StringSlice(copyChildren))
	garden := make(Garden, len(children))
	rows := make([][]rune, 2)
	rowsAndWindows := strings.Split(diagram, "\n")
	if len(rowsAndWindows) != 3 {
		return nil, errors.New("wrong diagram format")
	}
	for i, row := range rowsAndWindows[1:] {
		if len(row)%2 != 0 {
			return nil, errors.New("odd number of cups")
		}
		rows[i] = []rune(row)
	}
	if len(rows[0]) != len(rows[1]) {
		return nil, errors.New("mismatching rows")
	}
	for i, child := range copyChildren {
		plants := make([]string, 4)
		plants0, ok0 := Plant(rows[0][2*i])
		plants1, ok1 := Plant(rows[0][2*i+1])
		plants2, ok2 := Plant(rows[1][2*i])
		plants3, ok3 := Plant(rows[1][2*i+1])
		if !(ok0 && ok1 && ok2 && ok3) {
			return nil, errors.New("invalid cup codes")
		}
		plants[0] = plants0
		plants[1] = plants1
		plants[2] = plants2
		plants[3] = plants3
		_, ok := garden[child]
		if ok {
			return nil, errors.New("duplicate names")
		}
		garden[child] = plants

	}
	return &garden, nil
}

func (g *Garden) Plants(child string) ([]string, bool) {
	garden := *g
	plants, ok := garden[child]
	if ok {
		return plants, ok
	} else {
		return nil, ok
	}
}
