package prime

func Factors(n int64) []int64 {
	factors := make([]int64, 0)
	new_number := n
	for i := int64(2); i*i <= n; i++ {
		for new_number%i == 0 {
			factors = append(factors, i)
			new_number /= i
		}
	}
	if new_number != 1 {
		factors = append(factors, new_number)
	}
	return factors
}
