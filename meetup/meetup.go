package meetup

import (
	"time"
)

// Define the WeekSchedule type here.
type WeekSchedule int

const (
	First = iota
	Second
	Third
	Fourth
	Teenth
	Last
)

func Day(wSched WeekSchedule, wDay time.Weekday, month time.Month, year int) int {
	start := time.Date(year, month, 1, 0, 0, 0, 0, time.UTC)
	end := start.AddDate(0, 1, -1)
	days := make([]int, 0)
	for d := start; !d.After(end); d = d.AddDate(0, 0, 1) {
		if d.Weekday() == wDay {
			days = append(days, d.Day())
		}
	}

	switch wSched {
	case First, Second, Third, Fourth:
		return days[wSched]
	case Last:
		return days[len(days)-1]
	case Teenth:
		for _, day := range days {
			if day >= 13 && day <= 19 {
				return day
			}
		}
	}
	return 0
}
