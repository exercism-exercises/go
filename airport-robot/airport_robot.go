package airportrobot

import "fmt"

// Write your code here.
// This exercise does not have tests for each individual task.
// Try to solve all the tasks first before running the tests.
type Greeter interface {
	LanguageName() string
	Greet(visitor string) string
}

func SayHello(visitor string, greeter Greeter) string {
	return fmt.Sprintf("I can speak %s: %s", greeter.LanguageName(), greeter.Greet(visitor))
}

// 2.
type Italian struct {
}

func (italian Italian) LanguageName() string {
	return "Italian"
}

func (italian Italian) Greet(visitor string) string {
	return fmt.Sprintf("Ciao %s!", visitor)
}

// 3.
type Portuguese struct {
}

func (portuguese Portuguese) LanguageName() string {
	return "Portuguese"
}

func (portuguese Portuguese) Greet(visitor string) string {
	return fmt.Sprintf("Olá %s!", visitor)
}
