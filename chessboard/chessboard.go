package chessboard

// Declare a type named File which stores if a square is occupied by a piece - this will be a slice of bools
type File []bool

// Declare a type named Chessboard which contains a map of eight Files, accessed with keys from "A" to "H"
type Chessboard map[string]File

var fileNames []string = []string{"A", "B", "C", "D", "E", "F", "G", "H"}

// CountInFile returns how many squares are occupied in the chessboard,
// within the given file.
func CountInFile(cb Chessboard, fileName string) int {
	count := 0
	file, exists := cb[fileName]
	if exists {
		for _, isOccupied := range file {
			if isOccupied {
				count++
			}
		}
	}
	return count
}

// CountInRank returns how many squares are occupied in the chessboard,
// within the given rank.
func CountInRank(cb Chessboard, rank int) int {
	count := 0

	for _, fileName := range fileNames {
		file, exists := cb[fileName]
		if exists {
			if len(file) >= rank && rank > 0 && file[rank-1] {
				count++
			}
		}

	}

	return count
}

// CountAll should count how many squares are present in the chessboard.
func CountAll(cb Chessboard) int {
	return 64
}

// CountOccupied returns how many squares are occupied in the chessboard.
func CountOccupied(cb Chessboard) int {
	count := 0
	for _, fileName := range fileNames {
		count += CountInFile(cb, fileName)
	}
	return count
}
