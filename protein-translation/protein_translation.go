package protein

import "fmt"

/*
Below are the codons and resulting Amino Acids needed for the exercise.

| Codon              | Protein       |
| :----------------- | :------------ |
| AUG                | Methionine    |
| UUU, UUC           | Phenylalanine |
| UUA, UUG           | Leucine       |
| UCU, UCC, UCA, UCG | Serine        |
| UAU, UAC           | Tyrosine      |
| UGU, UGC           | Cysteine      |
| UGG                | Tryptophan    |
| UAA, UAG, UGA      | STOP          |
*/

var ErrStop = fmt.Errorf("STOP")
var ErrInvalidBase = fmt.Errorf("INVALID BASE")

func FromRNA(rna string) ([]string, error) {
	n := len(rna)
	result := make([]string, 0)
	for i := 0; i < n/3; i++ {
		aminoAcid, err := FromCodon(rna[i*3 : (i+1)*3])
		if err == ErrInvalidBase {
			return result, err
		} else if err == ErrStop {
			break
		} else {
			result = append(result, aminoAcid)
		}
	}

	return result, nil
}

func FromCodon(codon string) (string, error) {
	switch codon {
	case "AUG":
		return "Methionine", nil
	case "UUU", "UUC":
		return "Phenylalanine", nil
	case "UUA", "UUG":
		return "Leucine", nil
	case "UCU", "UCC", "UCA", "UCG":
		return "Serine", nil
	case "UAU", "UAC":
		return "Tyrosine", nil
	case "UGU", "UGC":
		return "Cysteine", nil
	case "UGG":
		return "Tryptophan", nil
	case "UAA", "UAG", "UGA":
		return "", ErrStop
	default:
		return "", ErrInvalidBase
	}
}
