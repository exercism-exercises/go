package wordy

import (
	"regexp"
	"strconv"
	"strings"
)

func Answer(question string) (int, bool) {
	overall := regexp.MustCompile(`What is (?P<base>-?[1-9][0-9]*)(?P<operations>( (plus (-?[1-9][0-9]*)|minus (-?[1-9][0-9]*)|multiplied by (-?[1-9][0-9]*)|divided by (-?[1-9][0-9]*)))*)(?P<powers>( raised to the (([1-9]*0th|[1-9]*1st|[1-9]*2nd|[1-9]*3rd|[1-9]*[4-9]th) power))*)\?`)
	if !overall.MatchString(question) {
		return 0, false
	}

	base := regexp.MustCompile(`What is (?P<base>-?[1-9][0-9]*)`)
	base_matches := strings.Split(base.FindString(question), " ")
	result, err := strconv.Atoi(base_matches[len(base_matches)-1])
	if err != nil {
		return 0, false
	}

	arithmetic := regexp.MustCompile(`(plus (-?[1-9][0-9]*)|minus (-?[1-9][0-9]*)|multiplied by (-?[1-9][0-9]*)|divided by (-?[1-9][0-9]*))`)
	arithmetic_matches := arithmetic.FindAllString(question, -1)
	if arithmetic_matches != nil {
		for _, expression := range arithmetic_matches {
			expressions := strings.Split(expression, " ")
			operator := expressions[0]
			value, err := strconv.Atoi(expressions[len(expressions)-1])
			if err != nil {
				return 0, false
			}
			switch operator {
			case "plus":
				result += value
			case "minus":
				result -= value
			case "multiplied":
				result *= value
			case "divided":
				result /= value
			}

		}
	}

	return result, true
}
