package hamming

import "errors"

func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return 0, errors.New("invalid case")
	}
	a_runes := []rune(a)
	b_runes := []rune(b)
	hamming_distance := 0
	for i, a_rune := range a_runes {
		if b_runes[i] != a_rune {
			hamming_distance += 1
		}
	}
	return hamming_distance, nil
}
