package isogram
import ( 
    "unicode"
	"strings"
) 

func IsIsogram(word string) bool {
	counter := make(map[rune] int)
	for _, r := range strings.ToLower(word)	{
		if !unicode.IsSpace(r) && r != '-' {
			_, ok := counter[r]
			if ok {
				return false
			} else {
				counter[r] = 1
			}
		}
	}
	return true
}
