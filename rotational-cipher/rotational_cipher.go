package rotationalcipher

import (
	"unicode"
)

var lowerCases string = "abcdefghijklmnopqrstuvwxyz"

func RotationalCipher(plain string, shiftKey int) string {
	indicesToLowerCases := []rune(lowerCases)
	lowerCasesToIndices := make(map[rune]int, 0)

	for i, c := range lowerCases {
		lowerCasesToIndices[c] = i
	}

	cipher := make([]rune, len(plain))
	for i, c := range plain {
		if unicode.IsLetter(c) {
			char := c
			if unicode.IsUpper(c) {
				char = unicode.ToLower(c)
			}
			k := lowerCasesToIndices[char]
			k += shiftKey
			k %= 26
			newChar := indicesToLowerCases[k]
			if unicode.IsUpper(c) {
				newChar = unicode.ToUpper(newChar)
			}
			cipher[i] = newChar
		} else {
			cipher[i] = c
		}
	}
	return string(cipher)
}
